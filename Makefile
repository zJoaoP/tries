%.o: %.c
	gcc -c $< -o $@

all: main.o tries.o
	gcc $^ -o Tries

clean:
	rm -f *.o
	@echo "Objetos limpos"