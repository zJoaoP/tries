#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tries.h"

#define Q_WORDS 49

char r_words[Q_WORDS][15] = {	"and", "array", "asm", "begin", "case", "const", "constructor", "destructor", "div", "do", "downto", "else", "end", "file", "for", "foward", "function",
								"goto", "if", "implementation", "in", "inline", "interface", "label", "mod", "nil", "not", "object", "of", "or", "packed", "procedure", "program",
								"record", "repeat", "set", "shl", "shr", "string", "then", "to", "type", "unit", "until", "uses", "var", "while", "with", "xor"};

struct node *tree = NULL;

int main(){
	int i;
	char *str = malloc(sizeof(char)*15);
	initNode(&tree, '_');

	for(i = 0; i < Q_WORDS; i++)
		insertString(&tree, r_words[i], strlen(r_words[i]));

	printTree(tree);
	printf("\n");

	printf("Digite uma palavra ou \"sair\" para sair do programa: ");
	while(scanf("%s", str) && strcmp(str, "sair") != 0){
		if(findString(tree, str, strlen(str)) == 0) printf("\"%s\" não é uma palavra reservada.\n\n", str);
		else printf("\"%s\" é uma palavra reservada!\n\n", str);

		printf("Digite uma palavra ou \"sair\" para sair do programa: ");
	}
	cutNode(&tree);
	free(str);
	return 0;
}