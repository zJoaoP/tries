#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tries.h"

void initNode(struct node **n, char prefix){
	*n = (struct node *) malloc(sizeof(struct node));
	(*n)->prefix = prefix;
	(*n)->head = NULL;
}

void cutNode(struct node **n){
	if(*n == NULL) return;

	cutNodeList(&((*n)->head));
	free(*n);
	*n = NULL;
}

void cutNodeList(struct nodeList **n){
	if(*n == NULL) return;

	cutNode(&((*n)->info));
	cutNodeList(&((*n)->next));
	free(*n);
	*n = NULL;
}

void initNodeList(struct nodeList **n, char info){
	struct node *leaf;
	initNode(&leaf, info);

	(*n) = (struct nodeList *) malloc(sizeof(struct nodeList));
	(*n)->info = leaf;
	(*n)->next = NULL;
}

void insertString(struct node **n, char string[], int size){
	struct nodeList *aux, *list;
	if(size == 0){
		if((*n)->head == NULL){
			initNodeList(&((*n)->head), '$');
			return;
		}

		list = (*n)->head;
		while(list != NULL && list->next != NULL && (list->info)->prefix != '$')
			list = list->next;

		if((list->info)->prefix == '$') return;
		else if(list == NULL) initNodeList(&list, '$');
		else initNodeList(&(list->next), '$');

		return;
	}

	aux = (*n)->head, list = aux;
	while(aux != NULL){
		if(((list)->info)->prefix == string[0]) break;
		list = aux;
		aux = aux->next;
	}

	if(list == NULL){
		initNodeList(&list, string[0]);
		(*n)->head = list;
		insertString(&(list->info), &string[1], size - 1);
	}
	else if(((list)->info)->prefix == string[0]) insertString(&((list)->info), &string[1], size - 1);	
	else{
		initNodeList(&aux, string[0]);
		list->next = aux;

		insertString(&(aux->info), &string[1], size - 1);
	}
}

void printTree(struct node *n){
	printf("%c", n->prefix);

	struct nodeList *list = n->head;
	while(list != NULL){
		printTree(list->info);
		list = list->next;
	}
}

int findString(struct node *n, char string[], int size){
	struct nodeList *list;
	if(size == 0){
		list = n->head;
		while(list != NULL){
			if((list->info)->prefix == '$') break;
			list = list->next;
		}
		return (list == NULL) ? 0 : 1;
	}
	
	list = n->head;
	while(list != NULL){
		if((list->info)->prefix == string[0]) break;
		list = list->next;
	}

	if(list == NULL) return 0;
	else findString(list->info, &string[1], size - 1);
}