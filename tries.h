#ifndef TRIES_H
#define TRIES_H 1

struct nodeList{
	struct node *info;
	struct nodeList *next;	
};

struct node{
	char prefix;
	struct nodeList *head;
};

void initNode(struct node **n, char info);
void cutNode(struct node **n);

void initNodeList(struct nodeList **n, char info);
void cutNodeList(struct nodeList **n);

void printTree(struct node *n);
void insertString(struct node **n, char string[], int size);

int findString(struct node *n, char string[], int size);

#endif